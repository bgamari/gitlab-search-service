{ mkDerivation, base, bytestring, lib, lucid, megaparsec, servant
, servant-lucid, servant-server, warp
}:
mkDerivation {
  pname = "gitlab-search-service";
  version = "0.1.0.0";
  src = ./.;
  isLibrary = false;
  isExecutable = true;
  executableHaskellDepends = [
    base bytestring lucid megaparsec servant servant-lucid
    servant-server warp
  ];
  license = lib.licenses.bsd3;
  mainProgram = "gitlab-search-service";
}
