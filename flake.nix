{
  description = "A simple improved search interface for GitLab";

  inputs.nixpkgs.url = "github:nixos/nixpkgs/nixos-22.11";

  outputs = {self, nixpkgs}:
    let
      system = "x86_64-linux";
      pkgs = nixpkgs.legacyPackages.${system};
    in {
      packages.${system}.default =
        pkgs.haskellPackages.callPackage (pkgs.lib.cleanSource ./.) {};
      nixosModules.default =
        { config, ... }:

        {
          systemd.services.gitlab-search-service = {
            description = "GitLab search service";
            script =
              let service = self.packages.${system}.default;
              in "${service}/bin/gitlab-search-service";
            wantedBy = [ "multi-user.target" ];
          };

          services.nginx = {
            virtualHosts."search.gitlab.${config.ghc.rootDomain}" = {
              enableACME = true;
              forceSSL = true;
              locations."/".proxyPass = "http://localhost:7003";
            };
          };
        };
    };
}
